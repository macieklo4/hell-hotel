This is a game titled "Hell Hotel". It's been made in Unity by a team of 7 people (2 programmers including myself). The game is a simple FPS in which a player must find an elevator to the next level and avoid being killed by ghosts. It can be downloaded using the link below (only on Windows)

link to the game - https://rickimanda.itch.io/hell-hotel
